package com.trytech.mongoocrawler.common;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by coliza on 2018/4/30.
 */
public class UrlUtil {
    public final static String COLON = ":";
    public final static String QUESTION_MARK = "?";
    public final static String DOUBLE_BACKSLASH = "//";

    public static String filterDomainAndPath(String url) throws MalformedURLException {
        URL urlObj = new URL(url);
        StringBuilder exactUrl = new StringBuilder();
        exactUrl.append(urlObj.getProtocol())
                .append(COLON)
                .append(DOUBLE_BACKSLASH)
                .append(urlObj.getHost())
                .append(urlObj.getPort())
                .append(urlObj.getPath())
                .append(QUESTION_MARK)
                .append(urlObj.getQuery());
        return exactUrl.toString();
    }
}
