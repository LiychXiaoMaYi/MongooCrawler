package com.trytech.mongoocrawler.common.protocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月07日
 */
public enum ProtocolType {
    COMMAND(0, "命令"), URL(1, "url"), DATA(2, "爬取数据"), MONITOR(3, "监控信息");
    private int value;
    private String name;

    ProtocolType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int val() {
        return value;
    }

    public String getName() {
        return name;
    }

    public boolean equalsTo(ProtocolType type){
        return val() == type.val();
    }
}
