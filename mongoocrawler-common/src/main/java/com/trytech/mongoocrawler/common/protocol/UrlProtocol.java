package com.trytech.mongoocrawler.common.protocol;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by coliza on 2018/3/24.
 */
@Slf4j
public class UrlProtocol extends AbstractProtocol {
    /***
     * url
     */
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public URL getURL() throws MalformedURLException {
        return new URL(url);
    }

    @Override
    public ProtocolType getType() {
        return ProtocolType.URL;
    }

    @Override
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", getType().val());
        jsonObject.put("url", getUrl());
        return jsonObject.toJSONString();
    }
}
