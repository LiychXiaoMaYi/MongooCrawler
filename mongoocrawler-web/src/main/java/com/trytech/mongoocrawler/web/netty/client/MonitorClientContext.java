package com.trytech.mongoocrawler.web.netty.client;


import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.protocol.CrawlerTransferProtocol;

/**
 * Created by coliza on 2017/10/7.
 */
public class MonitorClientContext {
    private static MonitorTcpClient nettyTcpClient;

    private MonitorClientContext() {
    }

    public static MonitorClientContext newInstance() {
        MonitorClientContext monitorClientContext = new MonitorClientContext();
        nettyTcpClient = MonitorTcpClient.newInstance("127.0.0.1", 8889);
        return monitorClientContext;
    }

    public <T extends AbstractProtocol> AbstractProtocol sendRequest(T t) {
        CrawlerTransferProtocol<T> transferProtocol = new CrawlerTransferProtocol();
        transferProtocol.setType(1);
        transferProtocol.setCls((Class<T>) t.getClass());
        transferProtocol.setContent(t);
        return nettyTcpClient.sendRequest(transferProtocol);
    }
}
