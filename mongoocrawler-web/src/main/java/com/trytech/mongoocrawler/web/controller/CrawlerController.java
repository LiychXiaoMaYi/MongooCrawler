package com.trytech.mongoocrawler.web.controller;

import com.trytech.mongoocrawler.common.protocol.CommandProtocol;
import com.trytech.mongoocrawler.web.common.RespStatus;
import com.trytech.mongoocrawler.web.vo.CommResp;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
@Controller
@RequestMapping("/crawler")
public class CrawlerController extends BaseController {
    @RequestMapping("/crawl")
    public @ResponseBody
    CommResp crawl() {
        CommandProtocol commandProtocol = new CommandProtocol();
        commandProtocol.setCommand(CommandProtocol.Command.CRAWL);
        commandProtocol.setTraceId(UUID.randomUUID().toString());
        commandProtocol.setData("https://cd.lianjia.com/ershoufang/rs/");
        try {
            sendRequest(commandProtocol);
            return CommResp.OK();
        } catch (Exception e) {
            return CommResp.resp(RespStatus.CRAWLER_SERVICE_ERROR);
        }

    }
}
