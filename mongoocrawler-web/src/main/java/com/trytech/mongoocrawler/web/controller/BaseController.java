package com.trytech.mongoocrawler.web.controller;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.web.netty.client.MonitorClientContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
public abstract class BaseController implements ApplicationContextAware {
    private MonitorClientContext monitorClientContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (monitorClientContext == null) {
            monitorClientContext = MonitorClientContext.newInstance();
        }
    }

    protected <T extends AbstractProtocol> AbstractProtocol sendRequest(T t) {
        return monitorClientContext.sendRequest(t);
    }
}
