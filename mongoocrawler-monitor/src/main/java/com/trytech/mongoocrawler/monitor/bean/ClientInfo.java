package com.trytech.mongoocrawler.monitor.bean;

import com.trytech.mongoocrawler.monitor.util.DateUtil;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by coliza on 2018/5/24.
 */
@Data
@NoArgsConstructor
public class ClientInfo {
    //ip地址
    private String ip;
    private ClientStatusEnum statusEnum;
    private String connectTime;
    private NioSocketChannel socketChannel;

    public ClientInfo(String ip, NioSocketChannel socketChannel) {
        this.ip = ip;
        this.statusEnum = ClientStatusEnum.FREE;
        this.connectTime = DateUtil.getNow();
        this.socketChannel = socketChannel;

    }

    enum ClientStatusEnum {
        FREE(1), RUNNING(2);
        private int status;

        ClientStatusEnum(int status) {
            this.status = status;
        }

        public int getStatus() {
            return this.status;
        }

        public boolean equalsTo(ClientStatusEnum to) {
            return this.status == to.getStatus();
        }
    }
}
