package com.trytech.mongoocrawler.monitor.protocol;

import com.alibaba.fastjson.JSONObject;
import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.protocol.ProtocolType;
import com.trytech.mongoocrawler.monitor.bean.MonitorData;

import java.util.UUID;

/**
 * Created by coliza on 2018/5/24.
 */
public class MonitorProtocol extends AbstractProtocol {

    public MonitorProtocol() {
        this.traceId = UUID.randomUUID().toString();
    }
    private MonitorData monitorData;

    public MonitorData getMonitorData() {
        return monitorData;
    }

    public void setMonitorData(MonitorData monitorData) {
        this.monitorData = monitorData;
    }

    @Override
    public ProtocolType getType() {
        return ProtocolType.MONITOR;
    }

    @Override
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", getType().val());
        jsonObject.put("monitorData", monitorData);
        return jsonObject.toJSONString();
    }
}
