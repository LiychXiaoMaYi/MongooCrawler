package com.trytech.mongoocrawler.client.transport.tcp;

import com.trytech.mongoocrawler.common.protocol.*;
import com.trytech.mongoocrawler.common.protocol.adapter.CommandAdapter;
import com.trytech.mongoocrawler.common.protocol.adapter.DataAdapter;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by coliza on 2018/3/24.
 */
public class TestServer {
    public static void main(String[] args) {
        //启动服务器端
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("decoder", new ProtocolDecoder());
                            ch.pipeline().addLast("encoder", new ProtocolEncoder());
                            ch.pipeline().addLast(new TestServer.SimpleServerHandler());
                        }
                    });

            ChannelFuture f = b.bind(8889).sync();
            f.channel().closeFuture().sync();
        } catch (Exception e) {

        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static class ProtocolProxy {
        private Map<Integer, AbstractProtocol> protocolMap = new HashMap<Integer, AbstractProtocol>();

        public ProtocolProxy() {
            protocolMap.put(ProtocolType.COMMAND.val(), new CommandProtocol());
            protocolMap.put(ProtocolType.URL.val(), new UrlProtocol());
            protocolMap.put(ProtocolType.DATA.val(), new HtmltextProtocol());
        }

        public AbstractProtocol getProtocol(ProtocolType protocolType) {
            return protocolMap.get(protocolType.val());
        }
    }

    public static class SimpleServerHandler extends SimpleChannelInboundHandler<CrawlerTransferProtocol> {
        @Override
        protected void channelRead0(ChannelHandlerContext ctx, CrawlerTransferProtocol transferProtocol) throws Exception {
            System.out.println("SimpleServerHandler.channelRead");
            AbstractProtocol protocol = null;
            Object obj = transferProtocol.getContent();
            if(obj instanceof CommandProtocol) {
                protocol = ((CommandProtocol)obj).resolve(new CommandAdapter<UrlProtocol>() {
                    @Override
                    public UrlProtocol handleProtocol(AbstractProtocol abstractProtocol, CommandProtocol.Command command) {
                        String url = "http://www.baidu.com";
                        System.out.println("开始发送url[" + url + "]");
                        UrlProtocol urlProtocol = new UrlProtocol();
                        urlProtocol.setTraceId("001");
                        urlProtocol.setUrl(url);
                        return urlProtocol;
                    }
                });
            }
            if(obj instanceof HtmltextProtocol) {
                protocol = ((HtmltextProtocol)obj).resolve(new DataAdapter<CommandProtocol>() {
                    @Override
                    public CommandProtocol handleProtocol(AbstractProtocol abstractProtocol, String data) {
                        System.out.println("结束通讯");
                        CommandProtocol commandProtocol = new CommandProtocol();
                        commandProtocol.setCommand(CommandProtocol.Command.OVER);
                        commandProtocol.setTraceId("JD90K38Y01");
                        return commandProtocol;
                    }
                });
            }
            // 向客户端发送消息
            // 在当前场景下，发送的数据必须转换成ByteBuf数组
            transferProtocol = new CrawlerTransferProtocol();
            transferProtocol.setContent(protocol);
            transferProtocol.setType(protocol.getType().val());
            transferProtocol.setCls(protocol.getClass());
            ctx.writeAndFlush(transferProtocol).sync();
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
            // 当出现异常就关闭连接
            cause.printStackTrace();
            ctx.close();
        }

        @Override
        public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
            ctx.flush();
        }
    }
}
