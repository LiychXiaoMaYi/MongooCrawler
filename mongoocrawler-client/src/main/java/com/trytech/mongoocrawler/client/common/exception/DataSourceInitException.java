package com.trytech.mongoocrawler.client.common.exception;

/**
 * Created by coliza on 2017/7/1.
 */
public class DataSourceInitException extends Exception {
    public DataSourceInitException() {
        super("DataSource initialization get failed!");
    }
    public DataSourceInitException(String message) {
        super(message);
    }
}
